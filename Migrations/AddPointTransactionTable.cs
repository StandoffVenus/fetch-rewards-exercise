using FetchRewards.Exercise.Web.Model;
using FluentMigrator;

namespace FetchRewards.Exercise.Web.Migrations
{
    [Migration(10)]
    public class AddPointTransactionTable : Migration
    {
        public override void Up()
        {
            Create
                .Table(nameof(PointTransaction))
                .WithColumn(nameof(PointTransaction.Id))
                    .AsInt32()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn(nameof(PointTransaction.PayerId))
                    .AsInt32()
                    .NotNullable()
                    .ForeignKey(nameof(Payer), nameof(Payer.Id))
                .WithColumn(nameof(PointTransaction.UserId))
                    .AsInt32()
                    .NotNullable()
                    .ForeignKey(nameof(User), nameof(User.Id))
                .WithColumn(nameof(PointTransaction.Time))
                    .AsDateTime()
                    .NotNullable()
                .WithColumn(nameof(PointTransaction.InitialPoints))
                    .AsInt32()
                    .NotNullable()
                .WithColumn(nameof(PointTransaction.RemainingPoints))
                    .AsInt32()
                    .NotNullable();
        }

        public override void Down()
        {
            Delete
                .Table(nameof(PointTransaction));
        }
    }
}