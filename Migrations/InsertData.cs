using System;
using FetchRewards.Exercise.Web.Model;
using FluentMigrator;

namespace FetchRewards.Exercise.Web.Migrations
{
    [Migration(1000)]
    public class InsertData : Migration
    {
        public override void Up()
        {
            Insert
                .IntoTable(nameof(User))
                .Row(new User
                {
                    Id = 1,
                    Name = "Liam",
                })
                .Row(new User
                {
                    Id = 2,
                    Name = "Sam",
                })
                .Row(new User
                {
                    Id = 3,
                    Name = "Andrew",
                });

            Insert
                .IntoTable(nameof(Payer))
                .Row(new Payer
                {
                    Id = 1,
                    Name = "MolsenCoors",
                })
                .Row(new Payer
                {
                    Id = 2,
                    Name = "Fetch Rewards!",
                })
                .Row(new Payer
                {
                    Id = 3,
                    Name = "Pop!_OS",
                });

            Insert
                .IntoTable(nameof(PointTransaction))
                .Row(new PointTransaction
                {
                    Id = 1,
                    UserId = 1,
                    PayerId = 1,
                    InitialPoints = 400,
                    RemainingPoints = 400,
                    Time = DateTime.Now.Subtract(TimeSpan.FromDays(1)),
                })
                .Row(new PointTransaction
                {
                    Id = 2,
                    UserId = 1,
                    PayerId = 2,
                    InitialPoints = 40,
                    RemainingPoints = 40,
                    Time = DateTime.Now.Subtract(TimeSpan.FromDays(2)),
                })
                .Row(new PointTransaction
                {
                    Id = 3,
                    UserId = 2,
                    PayerId = 3,
                    InitialPoints = 100,
                    RemainingPoints = 40,
                    Time = DateTime.Now.Subtract(TimeSpan.FromHours(6)),
                })
                .Row(new PointTransaction
                {
                    Id = 4,
                    UserId = 2,
                    PayerId = 3,
                    InitialPoints = 10,
                    RemainingPoints = 10,
                    Time = DateTime.Now,
                })
                .Row(new PointTransaction
                {
                    Id = 5,
                    UserId = 2,
                    PayerId = 3,
                    InitialPoints = -60,
                    RemainingPoints = 0,
                    Time = DateTime.Now.Subtract(TimeSpan.FromHours(3)),
                });
        }

        public override void Down()
        {
            Delete
                .FromTable(nameof(PointTransaction))
                .AllRows();
            Delete
                .FromTable(nameof(Payer))
                .AllRows();
            Delete
                .FromTable(nameof(User))
                .AllRows();
        }
    }
}