using FetchRewards.Exercise.Web.Model;
using FluentMigrator;

namespace FetchRewards.Exercise.Web.Migrations
{
    [Migration(0)]
    public class AddUserTable : Migration
    {
        public override void Up()
        {
            Create
                .Table(nameof(User))
                .WithColumn(nameof(User.Id))
                    .AsInt32()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn(nameof(User.Name))
                    .AsString(64)
                    .NotNullable()
                    .Unique();
        }

        public override void Down()
        {
            Delete
                .Table(nameof(User));
        }
    }
}