using FetchRewards.Exercise.Web.Model;
using FluentMigrator;

namespace FetchRewards.Exercise.Web.Migrations
{
    [Migration(100)]
    public class AddPayerTable : Migration
    {
        public override void Up()
        {
            Create
                .Table(nameof(Payer))
                .WithColumn(nameof(Payer.Id))
                    .AsInt32()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn(nameof(Payer.Name))
                    .AsString(64)
                    .NotNullable()
                    .Unique();
        }

        public override void Down()
        {
            Delete
                .Table(nameof(Payer));
        }
    }
}