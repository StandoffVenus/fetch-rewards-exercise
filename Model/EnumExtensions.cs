using System;

namespace FetchRewards.Exercise.Web.Model
{
    public static class EnumExtensions
    {
        public static string ValueToName<T>(this T @enum)
            where T : struct, Enum
            => Enum.GetName<T>(@enum) ?? string.Empty;
    }
}