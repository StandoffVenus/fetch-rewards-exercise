using System.Collections.Generic;
using System.Collections.Immutable;

namespace FetchRewards.Exercise.Web.Model.Result
{
    public class BalancesResult : IResult<BalancesReason>
    {
        public BalancesResult(IDictionary<int, int> balances)
        {
            this.Success = true;
            this.Balances = balances;
        }

        public BalancesResult(BalancesReason reason)
        {
            this.Success = false;
            this.Reason = reason;
        }

        public bool Success { get; }

        public BalancesReason? Reason { get; }

        public IDictionary<int, int> Balances { get; } = ImmutableDictionary<int, int>.Empty;
    }
}