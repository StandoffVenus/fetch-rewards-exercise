namespace FetchRewards.Exercise.Web.Model.Result
{
    public class UpdatePointsResult : IResult<UpdatePointsReason>
    {
        private UpdatePointsResult()
        {
            this.Success = true;
        }

        public UpdatePointsResult(UpdatePointsReason reason)
        {
            this.Success = false;
            this.Reason = reason;
        }

        public bool Success { get; }

        public UpdatePointsReason? Reason { get; }

        /// <summary>
        /// Represents the successful result.
        /// </summary>
        /// <remarks>
        /// Since there are no differences between successful results for <see cref="UpdatePointsResult" />,
        /// we use a static field to cache a single successful instance.
        /// </remarks>
        public static UpdatePointsResult SuccessfulResult = new UpdatePointsResult();
    }
}