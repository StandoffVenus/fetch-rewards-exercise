namespace FetchRewards.Exercise.Web.Model.Result
{
    public enum UpdatePointsReason
    {
        NoSuchUser,
        NoSuchPayer,
        InsufficientFunds,
        ErrorDuringUpdate,
    }
}