namespace FetchRewards.Exercise.Web.Model.Result
{
    public class TotalBalanceResult : IResult<TotalBalanceReason>
    {
        public TotalBalanceResult(int balance)
        {
            this.Success = true;
            this.Balance = balance;
        }

        public TotalBalanceResult(TotalBalanceReason reason)
        {
            this.Success = false;
            this.Reason = reason;
        }

        public bool Success { get; }

        public TotalBalanceReason? Reason { get; }

        public int Balance { get; } = 0;
    }
}