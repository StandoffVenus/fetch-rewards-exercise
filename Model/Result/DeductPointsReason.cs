namespace FetchRewards.Exercise.Web.Model.Result
{
    public enum DeductPointsReason
    {
        NoTransactionsFound,
        InsufficientFunds,
        ErrorDuringUpdate,
    }
}