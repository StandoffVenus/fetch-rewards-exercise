using System.Collections.Generic;
using System.Linq;

namespace FetchRewards.Exercise.Web.Model.Result
{
    public class DeductPointsResult : IResult<DeductPointsReason>
    {
        public DeductPointsResult(IEnumerable<PointTransaction> transactions)
        {
            this.Success = true;
            this.Transactions = transactions;
        }

        public DeductPointsResult(DeductPointsReason reason)
        {
            this.Success = false;
            this.Reason = reason;
        }

        public bool Success { get; }

        public DeductPointsReason? Reason { get; }

        public IEnumerable<PointTransaction> Transactions { get; } = Enumerable.Empty<PointTransaction>();
    }
}