using System;

namespace FetchRewards.Exercise.Web.Model.Result
{
    public interface IResult<T>
        where T : struct, Enum
    {
        /// <summary>
        /// Whether or not the action succeeded.
        /// </summary>
        /// <value>Gets the value of success as a boolean.</value>
        bool Success { get; }

        /// <summary>
        /// The reason the action failed. <see langword="null" /> if there was no failure.
        /// </summary>
        /// <value>Gets the reason for error as <typeparamref name="T" />.</value>
        T? Reason { get; }
    }
}