using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FetchRewards.Exercise.Web.Model
{
    [Table(nameof(User))]
    public class User
    {
        [Key]
        public int Id { get; set; }

        [StringLength(64)]
        public string Name { get; set; } = string.Empty;
    }
}