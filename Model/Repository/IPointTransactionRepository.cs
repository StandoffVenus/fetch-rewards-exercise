using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using FetchRewards.Exercise.Web.Model.Result;

namespace FetchRewards.Exercise.Web.Model.Repository
{
    public interface IPointTransactionRepository : IRepository<PointTransaction>
    {
        Task<IEnumerable<PointTransaction>> FetchPointTransactionsByUserId(int userId);

        Task<IEnumerable<PointTransaction>> FetchPointTransactionsByUserAndPayerId(int userId, int payerId);

        /// <summary>
        /// Fetches the total balance of the user.
        /// </summary>
        /// <param name="userId">The user (ID) whose balance to fetch.</param>
        /// <returns>A <see cref="System.Threading.Tasks.Task{TResult}" /> holding the details about the attempt (success or failure and why).</returns>
        Task<TotalBalanceResult> FetchTotalBalanceForUser(int userId);

        /// <summary>
        /// Fetches the total balances of the each of the user's payers.
        /// </summary>
        /// <param name="userId">The user (ID) whose payer balances to fetch.</param>
        /// <returns>A <see cref="System.Threading.Tasks.Task{TResult}" /> holding the details about the attempt (success or failure and why).</returns>
        Task<BalancesResult> FetchBalancesForUser(int userId);

        /// <summary>
        /// Tries to apply a point transaction for the user, succeeding if the payer exists and the transaction applied at
        /// the provided date will not negate their balance.
        /// </summary>
        /// <param name="transaction">The transaction to apply.</param>
        /// <param name="dbTransaction">The database transaction to apply the point transaction on.</param>
        /// <returns>A <see cref="System.Threading.Tasks.Task{TResult}" /> holding the details about the attempt (success or failure and why).</returns>
        Task<UpdatePointsResult> TryUpdatePointsForUser(PointTransaction transaction, IDbTransaction dbTransaction);

        /// <summary>
        /// Tries to deduct points from the user, succeeding if the user has a high enough balance.
        /// </summary>
        /// <param name="dbTransaction">The database transacton to deduct the points on.</param>
        /// <param name="userId">The user (ID) to deduct points from.</param>
        /// <param name="points">The points to deduct.</param>
        /// <returns>A <see cref="System.Threading.Tasks.Task{TResult}" /> holding the details about the attempt (success or failure and why).</returns>
        Task<DeductPointsResult> TryDeductPointsFromUser(int userId, int points, IDbTransaction dbTransaction);
    }
}