using Dapper;
using FetchRewards.Exercise.Web.Model.Result;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace FetchRewards.Exercise.Web.Model.Repository
{
    public class PointTransactionRepository : AbstractRepository<PointTransaction>, IPointTransactionRepository
    {
        public PointTransactionRepository(IDbConnection connection, IUserRepository userRepository, IPayerRepository payerRepository)
            : base(connection)
        {
            this.UserRepository = userRepository;
            this.PayerRepository = payerRepository;
        }

        private IUserRepository UserRepository { get; }

        private IPayerRepository PayerRepository { get; }

        public Task<IEnumerable<PointTransaction>> FetchPointTransactionsByUserId(int userId)
            => this.Connection.QueryAsync<PointTransaction>(
                $@"SELECT *
                     FROM {nameof(PointTransaction)}
                    WHERE {nameof(PointTransaction.UserId)} = @{nameof(userId)}",
                new { userId });

        public Task<IEnumerable<PointTransaction>> FetchPointTransactionsByUserAndPayerId(int userId, int payerId)
            => this.Connection.QueryAsync<PointTransaction>(
                $@"SELECT *
                     FROM {nameof(PointTransaction)}
                    WHERE {nameof(PointTransaction.UserId)} = @{nameof(userId)}
                      AND {nameof(PointTransaction.PayerId)} = @{nameof(payerId)}",
                new { userId, payerId });

        public async Task<TotalBalanceResult> FetchTotalBalanceForUser(int userId)
        {
            var transactions = await this.FetchPointTransactionsByUserId(userId);
            if (transactions.Count() == 0)
            {
                return new TotalBalanceResult(TotalBalanceReason.NoTransactionsFound);
            }

            var transactionsSum = transactions.Sum(x => x.RemainingPoints);

            return new TotalBalanceResult(transactionsSum);
        }

        public async Task<BalancesResult> FetchBalancesForUser(int userId)
        {
            var transactionGroups = (await this.FetchPointTransactionsByUserId(userId))
                .GroupBy(x => x.PayerId);

            if (transactionGroups.Count() == 0)
            {
                return new BalancesResult(BalancesReason.NoTransactionsFound);
            }

            // Sum each payer's group of transactions
            var transactionGroupSums = transactionGroups.ToDictionary(x => x.Key, x => x.Sum(y => y.RemainingPoints));

            return new BalancesResult(transactionGroupSums);
        }

        public async Task<UpdatePointsResult> TryUpdatePointsForUser(PointTransaction transactionUpdate, IDbTransaction dbTransaction)
        {
            var user = await this.UserRepository.FetchById(transactionUpdate.UserId);
            if (user == null)
            {
                return new UpdatePointsResult(UpdatePointsReason.NoSuchUser);
            }

            var payer = await this.PayerRepository.FetchById(transactionUpdate.PayerId);
            if (payer == null)
            {
                return new UpdatePointsResult(UpdatePointsReason.NoSuchPayer);
            }

            var payerTransactions = (await this.FetchPointTransactionsByUserAndPayerId(transactionUpdate.UserId, transactionUpdate.PayerId))
                .Where(x =>
                    x.Time <= transactionUpdate.Time &&
                    x.RemainingPoints > 0)
                .OrderBy(x => x.Time);

            // If this transaction will deduct points
            transactionUpdate.RemainingPoints = transactionUpdate.InitialPoints; // If the intial points are positive, we haven't spent any points yet
            if (transactionUpdate.InitialPoints < 0)
            {
                // Check if we have enough points to deduct
                if (payerTransactions.Sum(x => x.RemainingPoints) < -(transactionUpdate.InitialPoints))
                {
                    return new UpdatePointsResult(UpdatePointsReason.InsufficientFunds);
                };

                // Subtract from the previous transactions
                var pointsRemaining = -(transactionUpdate.InitialPoints);
                foreach (var transaction in payerTransactions)
                {
                    var amountToDeductFromRemainingPoints = Math.Min(transaction.RemainingPoints, pointsRemaining);
                    transaction.RemainingPoints -= amountToDeductFromRemainingPoints;
                    if (!(await this.Update(transaction, dbTransaction)))
                    {
                        return new UpdatePointsResult(UpdatePointsReason.ErrorDuringUpdate);
                    }

                    pointsRemaining -= amountToDeductFromRemainingPoints;
                    if (pointsRemaining <= 0)
                    {
                        break;
                    }
                }

                // Add the new transaction
                transactionUpdate.RemainingPoints = 0; // If the transaction is a deduction, then it will have no remaining points to spend
                if (await this.TryInsert(transactionUpdate, dbTransaction) == null)
                {
                    return new UpdatePointsResult(UpdatePointsReason.ErrorDuringUpdate);
                };
            }

            return UpdatePointsResult.SuccessfulResult;
        }

        public async Task<DeductPointsResult> TryDeductPointsFromUser(int userId, int points, IDbTransaction dbTransaction)
        {
            var transactions = (await this.FetchPointTransactionsByUserId(userId));
            if (transactions.Count() == 0)
            {
                return new DeductPointsResult(DeductPointsReason.NoTransactionsFound);
            }

            var remainingTransactions = transactions
                .Where(x => x.RemainingPoints > 0)
                .OrderBy(x => x.Time);
            if (remainingTransactions.Sum(x => x.RemainingPoints) < points)
            {
                return new DeductPointsResult(DeductPointsReason.InsufficientFunds);
            }

            var now = DateTime.Now;
            var pointsRemaining = points;
            var deductingTransactions = new List<PointTransaction>();

            // Deduct the points from the user's transactions
            foreach (var transaction in remainingTransactions)
            {
                // Make a new transaction for each individual point deduction
                var newDeduction = new PointTransaction
                {
                    UserId = userId,
                    PayerId = transaction.PayerId,
                    // The number of points subtracted will be either the entire transaction or the number of points
                    // remaining to be deducted at this point.
                    InitialPoints = -Math.Min(transaction.RemainingPoints, pointsRemaining),
                    RemainingPoints = 0,
                    Time = now,
                };

                // InitialPoints will be negative here, hence the addition 
                transaction.RemainingPoints += newDeduction.InitialPoints;
                if (!(await this.Update(transaction, dbTransaction)))
                {
                    return new DeductPointsResult(DeductPointsReason.ErrorDuringUpdate);
                }

                // Save the deduction
                deductingTransactions.Add(newDeduction);
                if (await this.TryInsert(newDeduction, dbTransaction) == null)
                {
                    return new DeductPointsResult(DeductPointsReason.ErrorDuringUpdate);
                }

                // If the pointsRemaining are <= 0, we've deducted all the points
                // InitialPoints will be negative here, hence the addition
                pointsRemaining += newDeduction.InitialPoints;
                if (pointsRemaining <= 0)
                {
                    break;
                }
            }

            return new DeductPointsResult(deductingTransactions);
        }
    }
}