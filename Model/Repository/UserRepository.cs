using System.Data;

namespace FetchRewards.Exercise.Web.Model.Repository
{
    public class UserRepository : AbstractRepository<User>, IUserRepository
    {
        public UserRepository(IDbConnection connection)
            : base(connection)
        { }
    }
}