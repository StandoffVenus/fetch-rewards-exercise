using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace FetchRewards.Exercise.Web.Model.Repository
{
    public class AbstractRepository<T> : IRepository<T>
        where T : class
    {
        public AbstractRepository(IDbConnection connection)
        {
            this.Connection = connection;
        }

        protected IDbConnection Connection { get; }

        public Task<T?> FetchById(int id)
            => this.Connection.GetAsync<T?>(id);

        public Task<IEnumerable<T>> FetchAll()
            => this.Connection.GetAllAsync<T>();

        public async Task<int?> TryInsert(T entity, IDbTransaction transaction)
        {
            try
            {
                return await this.Connection.InsertAsync(entity, transaction);
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> Update(T entity, IDbTransaction transaction)
        {
            try
            {
                return await this.Connection.UpdateAsync(entity, transaction);
            }
            catch
            {
                return false;
            }
        }
    }
}