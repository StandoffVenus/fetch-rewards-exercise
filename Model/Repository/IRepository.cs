using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace FetchRewards.Exercise.Web.Model.Repository
{
    public interface IRepository<T>
        where T : class
    {
        Task<T?> FetchById(int id);

        Task<IEnumerable<T>> FetchAll();

        /// <returns>The inserted entity's generated ID. Will be <see langword="null" /> if insert fails.</returns>
        Task<int?> TryInsert(T entity, IDbTransaction transaction);

        Task<bool> Update(T entity, IDbTransaction transaction);
    }
}