namespace FetchRewards.Exercise.Web.Model.Repository
{
    public interface IPayerRepository : IRepository<Payer>
    { }
}