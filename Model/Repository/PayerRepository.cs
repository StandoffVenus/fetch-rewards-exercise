using System.Data;

namespace FetchRewards.Exercise.Web.Model.Repository
{
    public class PayerReposiory : AbstractRepository<Payer>, IPayerRepository
    {
        public PayerReposiory(IDbConnection connection)
            : base(connection)
        { }
    }
}