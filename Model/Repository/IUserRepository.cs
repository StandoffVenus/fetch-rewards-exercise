namespace FetchRewards.Exercise.Web.Model.Repository
{
    public interface IUserRepository : IRepository<User>
    { }
}