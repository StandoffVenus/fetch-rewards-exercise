using System.Data;

namespace FetchRewards.Exercise.Web.Model
{
    public static class IDbConnectionExtensions
    {
        public static IDbTransaction BeginPreparedTransaction(this IDbConnection connection)
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }

            return connection.BeginTransaction();
        }
    }
}