using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace FetchRewards.Exercise.Web.Model
{
    [Table(nameof(PointTransaction))]
    public class PointTransaction
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }

        [ForeignKey(nameof(Payer))]
        public int PayerId { get; set; }

        public int InitialPoints { get; set; }

        public int RemainingPoints { get; set; }

        public DateTime Time { get; set; }

    }
}