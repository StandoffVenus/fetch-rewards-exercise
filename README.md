# Fetch Rewards Exercise

Small web application developed as an exercise for Fetch Rewards. Once executed, you can find all the API documentation at `localhost:5000/` (just `localhost/` if using Docker)


## Running the application

First, navigate to the repository's root folder, e.g., on Linux and Mac, `cd "Fetch\ Rewards\ Exercise"` or, on Windows, `cd "Fetch Rewards Exercise"`.

### Executing with Docker
The Docker process is untested but should be usable (generated, default project settings). You can execute with Docker via terminal:

```console
docker build . -t fetch-rewards-web
docker run fetch-rewards-web:latest 80:5000 -e ASPNETCORE_ENVIRONMENT="Development" -e ASPNETCORE_URLS="http://+;"
```

If you're on Linux or Mac, prepend the Docker commands with `sudo`.

Typically, .NET images will have a "latest" tag. If the last command reports an error about not finding the image, use `docker images` to find the version tag that docker produced the image with.

### Executing with .NET CLI
This approach requires you to install the [.NET SDK](https://docs.microsoft.com/en-us/dotnet/core/install/). Once installed, run the following commands via your terminal:

```console
dotnet restore
dotnet build
dotnet run
```

You should now be able to access the web app.

### Executing with Visual Studio Code
Alternatively, you can choose to run the application with [Visual Studio Code](https://code.visualstudio.com/). There a few more prequisites for this:

* Install the [.NET SDK](https://docs.microsoft.com/en-us/dotnet/core/install/)
* Install [Visual Studio Code](https://code.visualstudio.com/download)
* Install the [C# extension](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp) for Visual Studio Code
* Open the root folder Visual Studio Code
* Use the keyboard shortcut `Ctrl+Shift+D" to open the Debug menu
* Select either `.NET Core Launch` from the drop down
* Start debugging

Debugging in Visual Studio Code will obviously give you a lot more testing flexibility, but requires much more software. If you need more information, you can find it [here](https://code.visualstudio.com/docs/editor/debugging).

Additionally, if you'd like to debug the `Docker .NET Core Launch`, you can find detailed instructions [here](https://code.visualstudio.com/docs/containers/overview).
