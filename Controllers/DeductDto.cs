namespace FetchRewards.Exercise.Web.Controllers
{
    public class DeductDto
    {
        public int UserId { get; set; }

        public int Points { get; set; }
    }
}