using System.Data;
using System.Threading.Tasks;
using FetchRewards.Exercise.Web.Model;
using FetchRewards.Exercise.Web.Model.Repository;
using FetchRewards.Exercise.Web.Model.Result;
using Microsoft.AspNetCore.Mvc;

namespace FetchRewards.Exercise.Web.Controllers
{
    [ApiController]
    [Route("points")]
    public class PointTransactionController : Controller
    {
        public PointTransactionController(IDbConnection connection, IPointTransactionRepository pointTransactionRepository)
        {
            this.Connection = connection;
            this.PointTransactionRepository = pointTransactionRepository;
        }

        private IDbConnection Connection { get; }

        private IPointTransactionRepository PointTransactionRepository { get; }

        /// <summary>
        /// Fetches all transactions.
        /// </summary>
        /// <returns>Each transaction as stored in the database.</returns>
        [HttpGet("")]
        public async Task<IActionResult> Get()
            => Ok(await this.PointTransactionRepository.FetchAll());

        /// <summary>
        /// Fetches an individual transaction by its ID.
        /// </summary>
        /// <param name="id">The transaction ID.</param>
        /// <returns>The corresponding transaction if found; otherwise, a 404 error.</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var transaction = await this.PointTransactionRepository.FetchById(id);
            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        /// <summary>
        /// Fetches transactions for a user.
        /// </summary>
        /// <param name="userId">The user's ID.</param>
        /// <returns>The transaction for the given user (if any).</returns>
        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetByUser(int userId)
            => Ok(await this.PointTransactionRepository.FetchPointTransactionsByUserId(userId));

        /// <summary>
        /// Fetches transactions via their user and payer ID's.
        /// </summary>
        /// <param name="userId">The user's ID.</param>
        /// <param name="payerId">The payer's ID.</param>
        /// <returns>The transactions for the user/payer pair.</returns>
        [HttpGet("user/{userId}/payer/{payerId}")]
        public async Task<IActionResult> GetByUserAndPayer(int userId, int payerId)
            => Ok(await this.PointTransactionRepository.FetchPointTransactionsByUserAndPayerId(userId, payerId));

        /// <summary>
        /// Fetches the balances of a user's payers - sums the remaining points of transactions of a user by each payer.
        /// </summary>
        /// <param name="userId">The user's ID.</param>
        /// <returns>The payers' balances if the user is found; otherwise, a 404 error.</returns>
        [HttpGet("balance/{userId}")]
        public async Task<IActionResult> GetBalance(int userId)
        {
            var balancesResult = await this.PointTransactionRepository.FetchBalancesForUser(userId);
            if (balancesResult.Success)
            {
                return Ok(balancesResult.Balances);
            }

            return NotFound();
        }

        /// <summary>
        /// Fetches a user's total balance - sum of all their transactions' remaining points.
        /// </summary>
        /// <param name="userId">The user's ID.</param>
        /// <returns>The user's total balance if the user is found; otherwise, a 404 error.</returns>
        [HttpGet("balance/total/{userId}")]
        public async Task<IActionResult> GetTotalBalance(int userId)
        {
            var balanceResult = await this.PointTransactionRepository.FetchTotalBalanceForUser(userId);
            if (balanceResult.Success)
            {
                return Ok(balanceResult.Balance);
            }

            return NotFound();
        }

        /// <summary>
        /// Adds a new transaction if possible.
        /// The provided transaction MUST contain all of <see cref="PointTransaction" />'s fields (PUT requests require entire object).
        /// Returns OK (200) if the transaction is added;
        /// if the user or payer does not exist, 404 error;
        /// if the request does not contain the necessary fields or there is an when adding the transaction,
        /// a 400 error as the request is assumed malformed.
        /// </summary>
        /// <param name="pointTransaction">The transaction to add.</param>
        /// <returns>
        /// <list>
        /// <item>OK (200) if the transaction is added.</item>
        /// <item>If the user or payer does not exist, 404 error.</item>
        /// <item>
        /// If the request does not contain the necessary fields or there is an when adding the transaction,
        /// a 400 error as the request is assumed malformed.
        /// </item>
        /// </list>
        /// </returns>
        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] PointTransaction pointTransaction)
        {
            using var dbTransaction = this.Connection.BeginPreparedTransaction();
            var updateResult = await this.PointTransactionRepository.TryUpdatePointsForUser(pointTransaction, dbTransaction);
            if (updateResult.Success)
            {
                dbTransaction.Commit();

                return Ok();
            }

            var reasonName = updateResult.Reason!.Value.ValueToName();
            switch (updateResult.Reason)
            {
                case UpdatePointsReason.NoSuchUser:
                case UpdatePointsReason.NoSuchPayer:
                    return NotFound(reasonName);
                default:
                    return BadRequest(reasonName);
            }
        }

        /// <summary>
        /// Attempts to deduct the provided number of points from a user.
        /// Returns OK (200) if the points can be deducted,
        /// 404 error if no transactions are found for the given user,
        /// and if there is an error processing the request, a 400 error assuming the request is bad.
        /// </summary>
        /// <param name="deductDto">The user and the points to deduct.</param>
        /// <returns>
        /// <list type="bullet">
        /// <item>OK (200) if the points can be deducted.</item>
        /// <item>404 error if no transactions are found for the given user.</item>
        /// <item>If there is an error processing the request, a 400 error assuming the request is bad.</item>
        /// </list>
        /// </returns>
        [HttpPatch("balance")]
        public async Task<IActionResult> Deduct([FromBody] DeductDto deductDto)
        {
            using var dbTransaction = this.Connection.BeginPreparedTransaction();
            var deductResult = await this.PointTransactionRepository.TryDeductPointsFromUser(deductDto.UserId, deductDto.Points, dbTransaction);
            if (deductResult.Success)
            {
                dbTransaction.Commit();

                return Ok(deductResult.Transactions);
            }

            var reasonName = deductResult.Reason!.Value.ValueToName();
            switch (deductResult.Reason)
            {
                case DeductPointsReason.NoTransactionsFound:
                    return NotFound(reasonName);
                default:
                    return BadRequest(reasonName);
            }
        }
    }
}