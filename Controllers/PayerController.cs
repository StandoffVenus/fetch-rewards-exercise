using System.Data;
using System.Threading.Tasks;
using FetchRewards.Exercise.Web.Model;
using FetchRewards.Exercise.Web.Model.Repository;
using Microsoft.AspNetCore.Mvc;

namespace FetchRewards.Exercise.Web.Controllers
{
    [ApiController]
    [Route("payer")]
    public class PayerController : Controller
    {
        public PayerController(IDbConnection connection, IPayerRepository payerRepository)
        {
            this.Connection = connection;
            this.PayerRepository = payerRepository;
        }

        private IDbConnection Connection { get; }

        private IPayerRepository PayerRepository { get; }

        /// <summary>
        /// Fetches all stored payer entities.
        /// </summary>
        /// <returns>Every payer in the database.</returns>
        [HttpGet("")]
        public async Task<IActionResult> Get()
            => Ok(await this.PayerRepository.FetchAll());

        /// <summary>
        /// Fetches a payer by ID.
        /// </summary>
        /// <param name="id">The payer's ID.</param>
        /// <returns>The payer if found; otherwise, 404 error.</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var payer = await this.PayerRepository.FetchById(id);
            if (payer == null)
            {
                return NotFound();
            }

            return Ok(payer);
        }

        /// <summary>
        /// Inserts a payer to the database if successful. Only requires payer's name, which cannot be empty. Returns 201 on success, 400 on error.
        /// </summary>
        /// <param name="payer">The payer to insert.</param>
        /// <returns>201 Created with the payer's location and resulting database entity on success; otherwise, request is assumed malformed - 400 error.</returns>
        [HttpPost("")]
        public async Task<IActionResult> Insert([FromBody] Payer payer)
        {
            if (string.IsNullOrEmpty(payer.Name))
            {
                return BadRequest();
            }

            using var transaction = this.Connection.BeginPreparedTransaction();
            var id = await this.PayerRepository.TryInsert(payer, transaction);
            if (id == null)
            {
                return BadRequest();
            }

            transaction.Commit();

            return Created($"/payer/{id}", payer);
        }

        /// <summary>
        /// Updates a payer entity.
        /// Because this is a PUT, the request MUST include the entire <see cref="Payer" /> object definition.
        /// Returns 204 on success, 400 on error.
        /// </summary>
        /// <param name="payer">The updated payer entity that will be copied to the database.</param>
        /// <returns>204 No Content on successful update; otherwise, request assumed malformed - 400 error.</returns>
        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] Payer payer)
        {
            if (string.IsNullOrEmpty(payer.Name))
            {
                return BadRequest();
            }

            using var transaction = this.Connection.BeginPreparedTransaction();
            var success = await this.PayerRepository.Update(payer, transaction);
            if (!success)
            {
                return BadRequest();
            }

            transaction.Commit();

            return NoContent();
        }
    }
}