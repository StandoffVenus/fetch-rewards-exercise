using System.Data;
using System.Threading.Tasks;
using FetchRewards.Exercise.Web.Model;
using FetchRewards.Exercise.Web.Model.Repository;
using Microsoft.AspNetCore.Mvc;

namespace FetchRewards.Exercise.Web.Controllers
{
    [ApiController]
    [Route("user")]
    public class UserController : Controller
    {
        public UserController(IDbConnection connection, IUserRepository userRepository)
        {
            this.Connection = connection;
            this.UserRepository = userRepository;
        }

        private IDbConnection Connection { get; }

        private IUserRepository UserRepository { get; }

        /// <summary>
        /// Fetches all stored user entities.
        /// </summary>
        /// <returns>Every user in the database.</returns>
        [HttpGet("")]
        public async Task<IActionResult> Get()
            => Ok(await this.UserRepository.FetchAll());

        /// <summary>
        /// Fetches a user by ID.
        /// </summary>
        /// <param name="id">The user's ID.</param>
        /// <returns>The user if found; otherwise, 404 error.</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var user = await this.UserRepository.FetchById(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        /// <summary>
        /// Inserts a user to the database if successful. Only requires user's name, which cannot be empty. Returns 201 on success, 400 on error.
        /// </summary>
        /// <param name="user">The user to insert.</param>
        /// <returns>201 Created with the user's location and resulting database entity on success; otherwise, request is assumed malformed - 400 error.</returns>
        [HttpPost("")]
        public async Task<IActionResult> Insert([FromBody] User user)
        {
            if (string.IsNullOrEmpty(user.Name))
            {
                return BadRequest();
            }

            using var transaction = this.Connection.BeginPreparedTransaction();
            var id = await this.UserRepository.TryInsert(user, transaction);
            if (id == null)
            {
                return BadRequest();
            }

            transaction.Commit();

            return Created($"/user/{id}", user);
        }

        /// <summary>
        /// Updates a user entity.
        /// Because this is a PUT, the request MUST include the entire <see cref="User" /> object definition.
        /// Returns 201 on success, 400 on error.
        /// </summary>
        /// <param name="user">The updated user entity that will be copied to the database.</param>
        /// <returns>204 No Content on successful update; otherwise, request assumed malformed - 400 error.</returns>
        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] User user)
        {
            if (string.IsNullOrEmpty(user.Name))
            {
                return BadRequest();
            }

            using var transaction = this.Connection.BeginPreparedTransaction();
            var success = await this.UserRepository.Update(user, transaction);
            if (!success)
            {
                return BadRequest();
            }

            transaction.Commit();

            return NoContent();
        }
    }
}