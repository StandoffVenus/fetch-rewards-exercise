using System;
using System.Data;
using System.IO;
using System.Reflection;
using FetchRewards.Exercise.Web.Migrations;
using FetchRewards.Exercise.Web.Model.Repository;
using FluentMigrator.Runner;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace FetchRewards.Exercise.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Fetch Rewards Application Exercise", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            var sqliteSettings = new SqliteConnectionStringBuilder
            {
                DataSource = Path.GetTempFileName(),
                Mode = SqliteOpenMode.ReadWriteCreate,
            };
            var memoryDatabaseConnection = new SqliteConnection(sqliteSettings.ToString());

            services
                .AddSingleton<IDbConnection>(memoryDatabaseConnection)
                .AddTransient<IUserRepository, UserRepository>()
                .AddTransient<IPointTransactionRepository, PointTransactionRepository>()
                .AddTransient<IPayerRepository, PayerReposiory>()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSQLite()
                    .WithGlobalConnectionString(memoryDatabaseConnection.ConnectionString)
                    .WithMigrationsIn(typeof(AddUserTable).Assembly))
                .AddLogging(lb => lb.AddFluentMigratorConsole());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Migrate database
            using var temporaryScope = app.ApplicationServices.CreateScope();
            temporaryScope.ServiceProvider
                .GetRequiredService<IMigrationRunner>()
                .MigrateUp();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Fetch Rewards Application Exercise");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
